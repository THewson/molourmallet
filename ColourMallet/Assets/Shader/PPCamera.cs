﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PPCamera : MonoBehaviour
{
    public Material mat;
    public float fadeTime;
    public float currentFadeTime;
    public bool fadingIn;
    public bool fading;

    // Called by the camera to apply the image effect
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (fadingIn)
        {
            if (fading)
            {
                currentFadeTime -= Time.deltaTime;
                float f = currentFadeTime / fadeTime;
                mat.SetFloat("_FadeInOut", f);

                if (currentFadeTime <= 0)
                {
                    fading = false;
                    currentFadeTime = 0;
                }
            }
            else
            {
                mat.SetFloat("_FadeInOut", 0.0f);
            }
        }
        else
        {
            if (fading)
            {
                currentFadeTime -= Time.deltaTime;
                float f = 1 - (currentFadeTime / fadeTime);
                mat.SetFloat("_FadeInOut", f);

                if (currentFadeTime <= 0)
                {
                    fading = false;
                    currentFadeTime = 0;
                }
            }
            else
            {
                mat.SetFloat("_FadeInOut", 1.0f);
            }
        }
        //mat is the material containing your shader
        Graphics.Blit(source, destination, mat);
    }

    /// <summary>
    /// Determines how long to transition the fade and if it's going dark, or returning to normal.
    /// </summary>
    /// <param name="timeTillFade"></param>
    /// <param name="_fadingIn"></param>
    public void Fade(float fadingTime, bool _fadingIn)
    {
        fadingIn = _fadingIn;
        fadeTime = fadingTime;
        currentFadeTime = fadingTime;
        fading = true;
    }

    public void FadeIn(float fadingTime)
    {
        Fade(fadingTime, true);
    }

    public void FadeOut(float fadingTime)
    {
        Fade(fadingTime, false);
    }
}
