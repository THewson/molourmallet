﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/GrayScale" {
    Properties{
     _MainTex("", 2D) = "white" {}
     _FadeInOut("Effect", Range(0.0, 1.0)) = 0.5
    }

        SubShader{

        ZTest Always Cull Off ZWrite Off Fog { Mode Off } //Rendering settings

         Pass{
          CGPROGRAM
          #pragma vertex vert
          #pragma fragment frag
          #include "UnityCG.cginc" 
          //we include "UnityCG.cginc" to use the appdata_img struct

          struct v2f {
           float4 pos : POSITION;
           half2 uv : TEXCOORD0;
          };

    //Our Vertex Shader 
    v2f vert(appdata_img v) {
     v2f o;
     o.pos = UnityObjectToClipPos(v.vertex);
     o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord.xy);
     return o;
    }

    sampler2D _MainTex; //Reference in Pass is necessary to let us use this variable in shaders
    uniform float _FadeInOut;

    //Our Fragment Shader
    fixed4 frag(v2f i) : COLOR{
    fixed4 orgCol = tex2D(_MainTex, i.uv); //Get the orginal rendered color 

    fixed4 col = fixed4(0, 0, 0, 1);

    if (_FadeInOut != 1) {

        //Make changes on the color
        float avg = (orgCol.r + orgCol.g + orgCol.b) / 3.0f;
        float r = ((orgCol.r * _FadeInOut) + avg) / 2.0f;
        float g = ((orgCol.g * _FadeInOut) + avg) / 2.0f;
        float b = ((orgCol.b * _FadeInOut) + avg) / 2.0f;
         col = fixed4(r, g, b, 1);
    }
    else {
        col = orgCol;
    }
     return col;
    }
    ENDCG
   }
    }
        FallBack "Diffuse"
}