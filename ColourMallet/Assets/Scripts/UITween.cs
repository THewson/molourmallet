using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class UITween : MonoBehaviour
{

    [System.Serializable]
    public struct Tween
    {
        [Tooltip("The position the tween starts at")]
        public Vector3 s_StartPosition;

        [Tooltip("The position the tween ends at")]
        public Vector3 s_EndPosition;

        [Tooltip("The speed the tween moves at")]
        public float s_Speed;

        [Tooltip("How long this tween will take to activate")]
        public float s_TimeTillNextTween;

        [Tooltip("This happens at the moment the tween begins")]
        public UnityEvent s_StartEvent;
        [Tooltip("This happens at the end of a tween.")]
        public UnityEvent s_EndEvent;
    }


    [Tooltip("The time until the next tween activates")]
    public float m_TimeTilNextTween = 0;

    [Tooltip("The current active tween.")]
    public int m_CurrentTween = 0;

    [Tooltip("Progress of the current tween.")]
    public float m_T;

    public bool m_StartedNewTween = true;
    public bool m_Loop = true;

    public List<Tween> m_TweenList;

    // Start is called before the first frame update
    void Start()
    {
        m_TimeTilNextTween = m_TweenList[m_CurrentTween].s_TimeTillNextTween;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_CurrentTween < m_TweenList.Count && m_TimeTilNextTween <= 0)
        {
            if (m_StartedNewTween)
            {
                m_StartedNewTween = false;
                m_TweenList[m_CurrentTween].s_StartEvent.Invoke();
            }

            m_T += Time.deltaTime * m_TweenList[m_CurrentTween].s_Speed;
            // Moves the object to target position
            transform.GetComponent<RectTransform>().localPosition = Vector3.Lerp(m_TweenList[m_CurrentTween].s_StartPosition,
                                                                                 m_TweenList[m_CurrentTween].s_EndPosition,
                                                                                 m_T);

            // If the tween completes
            if (m_T >= 1 && m_CurrentTween <= m_TweenList.Count)
            {
                m_TweenList[m_CurrentTween].s_EndEvent.Invoke();
                m_StartedNewTween = true;
                m_CurrentTween++;
                m_T = 0;
                m_TimeTilNextTween = m_TweenList[m_CurrentTween - 1].s_TimeTillNextTween;

            }
        }
        else
        {
            if (m_Loop && m_CurrentTween >= m_TweenList.Count)
            {
                m_CurrentTween = 0;
            }
            else
            {

                if (m_CurrentTween >= m_TweenList.Count)
                {
                    Debug.Log("Complete");
                    m_CurrentTween = 0;
                    enabled = false;
                }
                else { m_TimeTilNextTween -= Time.deltaTime; }
            }
        }
    }
}
