﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteChecker : MonoBehaviour
{
    public AudioListener listener;
    public bool boolParam;
    private void OnEnable() {
        gameObject.SetActive(listener.enabled == boolParam);
    }
}
