﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class SplatCam : MonoBehaviour {
    public GameManager gameMan;
    public Hammer hammer;
    public int id;
    public Camera cam;
    public List<int> ids;
    public int progress;
    public int score;
    public Text scoreText;
    public UnityEvent OnNextLevel;
    public AudioSource SplatterSound;

    void Start() {
        cam = gameObject.GetComponent<Camera>();
        progress = -1;
    }

    public void SetTimeScale(float val) {
        Time.timeScale = val;
    }

    // Update is called once per frame
    void Update() {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0)) {
            Physics.Raycast(ray, out hit);
            Splatter splat = hit.transform.gameObject.GetComponent<Splatter>();
        if (splat != null) {
            Debug.Log(hit);
                //rotates the hammer to face the palette
                hammer.HammerDown(splat);
                if (splat != null) {
                    if (splat.id != id) {
                        gameMan.Lose();
                    } else {
                        splat.CreateSplatter();
                        SplatterSound.Play();
                        NextLevel();
                    }
                }
            }
        }
    }
    public void NextLevel() {
        progress++;
        if (progress >= ids.Count) {
            score++;
            gameMan.currentLevel++;
            gameMan.currentLevelDisplay.text = (gameMan.currentLevel).ToString();
            scoreText.text = "Score: " + score.ToString();
            progress = -1;
            OnNextLevel.Invoke();

        } else {
            id = ids[progress];
            hammer.NewColour();
        }
    }
}
