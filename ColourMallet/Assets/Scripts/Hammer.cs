﻿using System.Collections.Generic;
using UnityEngine;

public class Hammer : MonoBehaviour {
    public GameObject hammerObject;
    public GameManager gameMan;
    public List<Splatter> paintSplatters;
    public GameObject displayObject;
    public SplatCam spCam;

    public float swingSpeed;

    Splatter splatter;
    float progress;
    bool moving;
    bool goingDown;
    private Vector3 origin;

    private Vector3 target;
    private void Start() {
        moving = false;
        goingDown = false;
        origin = hammerObject.transform.position;
    }

    private void OnEnable() {
        HammerUp();
    }
    private void Update() {

        // controls moving
        if (moving) {
            //picks the direction to move
            if (goingDown) {
                progress += swingSpeed * Time.deltaTime;
                if (progress >= 1) {
                    progress = 1f;
                    HammerUp();
                }
            } else {
                progress -= swingSpeed * Time.deltaTime;
                if (progress <= 0) {
                    moving = false;
                    progress = 0f;
                }
            }
                hammerObject.transform.position = Vector3.Lerp(origin, target, progress);
                hammerObject.transform.localRotation = Quaternion.Euler(-90 * progress, 0, 90);
        }
    }

    // Spawns the colour the player must hit next
    public void NewColour() {
        if (displayObject != null) Destroy(displayObject);
        Splatter id = paintSplatters[spCam.id];
        displayObject = Instantiate(id.gameObject, transform);
        Destroy(displayObject.GetComponent<Splatter>());
        displayObject.transform.localPosition = new Vector3(0, 0, 0);
        displayObject.transform.localScale = new Vector3(25, 25, 25);
        displayObject.transform.localRotation = new Quaternion(90, 0, 0, 90);
    }

    public void HammerDown(Splatter newVal) {
        splatter = newVal;
        moving = true;
        goingDown = true;
        target = splatter.transform.position - new Vector3(0,0,-3);
    }
    public void HammerUp() {
        moving = true;
        goingDown = false;
    }
}
