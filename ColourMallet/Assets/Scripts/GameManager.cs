﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameManager : MonoBehaviour {
    public SplatCam spCam;
    public GameObject loseMenu;

    public List<Splatter> easyPaintSplatters;
    public List<Splatter> mediumPaintSplatters;
    public List<Splatter> hardPaintSplatters;

    public List<PalettePatternManager> easyPatternManagers;
    public List<PalettePatternManager> mediumPatternManagers;
    public List<PalettePatternManager> hardPatternManagers;
    [HideInInspector]
    public List<Splatter> paintSplatters = new List<Splatter>();

    public Hammer hammerManager;

    [Header("Time properties")]

    [Tooltip("How long the first level is")]
    public float initialTime;
    [Tooltip("How much time decreases each level")]
    public float timerDecrease;
    [Tooltip("The lowest amount of time a player has (highest difficulties")]
    public float minimumTime;
    [Tooltip("how many levels before the timer decreases")]
    public int timeDecriment;
    public Image timeBar;
    public int currentLevel;
    public Text currentLevelDisplay;


    [Header("")]
    public UnityEvent OnLose;

    private float currentTime;

    bool isLost = false;

    public enum Difficulty {
        EASY,
        MEDIUM,
        HARD
    }

    [Header("Difficulty properties")]

    [Tooltip("The starting difficulty of the selected palettes")]
    public Difficulty difficulty;
    [Tooltip("The amount of levels before the difficulty increases")]
    public int levelsToIncrease;

    float timer;
    int lCounter;
    int dCounter;

    public void Initialise() {

        spCam.score = 0;
        currentLevel = 1;
        dCounter = 0;
        lCounter = -1;
        difficulty = Difficulty.EASY;
        StartLevel();
    }

    public void Cleanup() {
        Splatter[] blocks = FindObjectsOfType(typeof(Splatter)) as Splatter[];
        foreach (Splatter go in blocks) {
            Destroy(go.gameObject);
        }
        GameObject[] splats = GameObject.FindGameObjectsWithTag("SplatEffect");
        foreach (GameObject go in splats) {
            Destroy(go.gameObject);
        }
    }

    public void StartLevel() {
        currentTime = initialTime;
        spCam.progress = -1;

        isLost = false;
        Cleanup();
        // LIST SETUP
        // resets the splatterList and picks the lists to incorporate
        paintSplatters = new List<Splatter>();


        switch (difficulty) {
            case Difficulty.HARD:
                foreach (Splatter sp in hardPaintSplatters) paintSplatters.Add(sp);
                foreach (Splatter sp in mediumPaintSplatters) paintSplatters.Add(sp);
                foreach (Splatter sp in easyPaintSplatters) paintSplatters.Add(sp);
                break;
            case Difficulty.MEDIUM:
                foreach (Splatter sp in mediumPaintSplatters) paintSplatters.Add(sp);
                foreach (Splatter sp in easyPaintSplatters) paintSplatters.Add(sp);
                break;
            case Difficulty.EASY:
                foreach (Splatter sp in easyPaintSplatters) paintSplatters.Add(sp);
                break;
        }

        // shuffles the splatterlist
        paintSplatters = paintSplatters.OrderBy(a => System.Guid.NewGuid()).ToList();

        hammerManager.paintSplatters = paintSplatters;
        SelectManager();

        // Increments the counters for increasing difficulty
        lCounter++;
        if (lCounter >= timeDecriment) {
            lCounter = 0;
            currentTime -= timerDecrease;
            if (currentTime < minimumTime) currentTime = minimumTime;
        }
        dCounter++;
        if (dCounter >= levelsToIncrease) {
            switch (difficulty) {
                case (Difficulty.EASY):
                    difficulty = Difficulty.MEDIUM;
                    dCounter = 0;
                    break;
                case (Difficulty.MEDIUM):
                    difficulty = Difficulty.HARD;
                    dCounter = 0;
                    break;
            }
        }
        timer = 0;
        if (spCam.score > PlayerPrefs.GetInt("highscore"))
            PlayerPrefs.SetInt("highscore", spCam.score);

        spCam.NextLevel();
    }

    private void Update() {
        timer += Time.deltaTime;
        timeBar.fillAmount = timer / currentTime;
        if (timer >= currentTime && !isLost) Lose();
    }

    public void Lose() {
        isLost = true;
        loseMenu.SetActive(true);
        currentLevel = 0;
        currentLevelDisplay.text = "0";
        OnLose.Invoke();
    }

    void SelectManager() {
            int r = 0;
        switch (difficulty) {
            case (Difficulty.EASY):
                r = Random.Range(0, easyPatternManagers.Count);
                easyPatternManagers[r].paintSplatters = paintSplatters;
                easyPatternManagers[r].spCam = spCam;
                easyPatternManagers[r].GenerateLevel();
                break;
            case (Difficulty.MEDIUM):
                r = Random.Range(0, mediumPatternManagers.Count);
                mediumPatternManagers[r].paintSplatters = paintSplatters;
                mediumPatternManagers[r].spCam = spCam;
                mediumPatternManagers[r].GenerateLevel();
                break;
            case (Difficulty.HARD):
                r = Random.Range(0, hardPatternManagers.Count);
                hardPatternManagers[r].paintSplatters = paintSplatters;
                hardPatternManagers[r].spCam = spCam;
                hardPatternManagers[r].GenerateLevel();
                break;
        }
    }
}
