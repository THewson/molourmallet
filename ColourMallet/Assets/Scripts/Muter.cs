﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muter : MonoBehaviour
{
    public AudioListener audioListener;
    public void SwapMute() {
        if (AudioListener.pause) {
            //AudioListener.pause = false;
            AudioListener.volume = 1.0f;
        } else {
            //AudioListener.pause = true;
            AudioListener.volume = 0.0f;

        }
    }
}
