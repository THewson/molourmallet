﻿using System.Collections.Generic;
using UnityEngine;

public class Splatter : MonoBehaviour {
    public int id;
    public List<GameObject> particleEffects;
    public List<GameObject> splatEffects;

    public void CreateSplatter() {
        int r = Random.Range(0, particleEffects.Count);
        Instantiate(particleEffects[r], transform.position, particleEffects[r].transform.rotation);

        r = Random.Range(0, splatEffects.Count);

        GameObject spr = Instantiate(splatEffects[r], transform.position, splatEffects[r].transform.rotation);

        // randomises the rotation of the splatter
        float ran = Random.Range(0, 360);
        spr.transform.Rotate(new Vector3(0, 0, ran));

        spr.GetComponent<MeshRenderer>().material.color = gameObject.GetComponent<MeshRenderer>().material.color;
        Destroy(this.gameObject);
    }
}
