﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PalettePatternManager : MonoBehaviour {
    public List<Transform> PaintPositions;

    [HideInInspector]
    public GameManager gMan;

    [HideInInspector]
    public SplatCam spCam;

    [HideInInspector]
    public List<Splatter> paintSplatters;

    public void GenerateLevel() {
        List<int> splatterID = new List<int>();
        for (int i = 0; i < PaintPositions.Count; i++) {
            int newId = i;
            splatterID.Add(newId);
        }

        // places the paint splats in a random order
        splatterID = splatterID.OrderBy(a => Random.value).ToList();
        for (int i = 0; i < PaintPositions.Count; i++) {
            Splatter ps = Instantiate(paintSplatters[splatterID[i]]);
            ps.id = splatterID[i];
            ps.transform.position = PaintPositions[i].position;
//            ps.gameMan = gMan;
        }

        splatterID = splatterID.OrderBy(a => Random.value).ToList();
        spCam.ids = splatterID;

    }
}
