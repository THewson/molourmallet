﻿using UnityEngine;
using UnityEngine.UI;
public class HighScoreDisplay : MonoBehaviour {
    Text text;
    void Start() {
        text = GetComponent<Text>();
        text.text = "HIGHSCORE: " + PlayerPrefs.GetInt("highscore").ToString();
    }

    private void OnEnable() {
            text = GetComponent<Text>();
            text.text = "HIGHSCORE: " + PlayerPrefs.GetInt("highscore").ToString();
    }

    public void ResetHighscore() {
        PlayerPrefs.SetInt("highscore", 0);
        text.text = "HIGHSCORE: " + PlayerPrefs.GetInt("highscore").ToString();
    }
}