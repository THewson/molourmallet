﻿using UnityEngine;
using UnityEngine.Advertisements;

public class AdvertManager : MonoBehaviour {
    public float secondsToAdShow;
    public bool testMode = true;
    public string gameId = "b3da98b4-db6f-4078-aa6b-84ab58e04e54";
	float secondsRemaining;

    // Start is called before the first frame update
    void Start() {
        Advertisement.Initialize(gameId, testMode);
		secondsRemaining = secondsToAdShow;
    }
    private void Update() {
        secondsRemaining -= Time.deltaTime;
    }
    public void DisplayAd() {
        if (secondsRemaining <= 0) {
            Advertisement.Show();
			secondsRemaining = secondsToAdShow;
        }
    }
}
